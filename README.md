# Identifikationsnummern von Druckwerken des Verlags Gebauer

Durch die Bestrebungen der letzten Jahrzehnte, eine retrospektive Nationalbibliographie für den deutschen Sprachraum zu schaffen, und nicht zuletzt dank dem als Digitalisatenkatalog konzipierten [VD18](http://uri.gbv.de/database/vd18) sind die Veröffentlichungen der halleschen Druckereien und Verlage zur Zeit der Aufklärung inzwischen weitreichend erschlossen und digital zugänglich.

Von diesen Quellen ausgehend wurden umfassend [Katalogisate zu Druckwerken des Verlags Gebauer](https://gitlab.informatik.uni-halle.de/dikon/vkg-pica) bereitgestellt, die bibliothekarischen Identifikationsnummern der einzelnen Datensätze extrahiert und hier schließlich getrennt nach Katalogen und Publikationsgattungen in separaten [JSON-Dateien](./data/) verzeichnet [\*].

Orientierung bei der Frage nach bisher nicht erfolgten Erschließungen und/oder Digitalisierungen bietet die [Verlagsbibliographie Gebauer](https://gitlab.informatik.uni-halle.de/dikon/vag-bib).

---

[\*] Für Katalogisate des VD18 wurde auf die VD18-Nummer zurückgegriffen, für Katalogisate des GVK auf die Pica-Produktionsnummer (PPN).
