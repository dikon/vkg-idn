#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Creator:          D. Herre
GitLab:      dikon/vkg-idn

Created:        2020-07-07
Last Modified:  2020-07-26
"""

from librair import schemas
from librair import interfaces

online = schemas.json.reader("data/vd18-online--printer-gebauer.json")
vd18 = interfaces.unapi.Client("http://unapi.gbv.de", "vd18", "ppn")
vd18_sru = interfaces.sru.Client("http://sru.gbv.de/vd18")

online_id = []
online_data = []
online_years = []
online_titles = []
online_publisher = []
counter = 0
max_year = -1

FORMAT = "jsmf-json"


def vdn_query(vd18_nr):
    return "pica.vdn={0}".format(vd18_nr)


def get_ppn(vd18_nr, bbg="O"):
    items = vd18_sru.search(vdn_query(vd18_nr), schema="picaxml", records=5)
    if type(items) == list:
        for item in items:
            bbg_val = extract_bbg(item)
            if bbg_val.startswith(bbg):
                item_ppn = extract_ppn(item)
                if item_ppn != "":
                    return item_ppn
    else:
        bbg_val = extract_bbg(items)
        if bbg_val.startswith(bbg):
            item_ppn = extract_ppn(items)
            if item_ppn != "":
                return item_ppn
    return None


def extract_bbg(item):
    bbg_val = item.find(".//datafield[@tag='002@']/subfield[@code='0']",
                        namespaces=item.nsmap)
    if bbg_val is not None:
        if bbg_val.text is not None:
            return bbg_val.text
    return ""


def extract_ppn(item):
    ppn_val = item.find(".//datafield[@tag='003@']/subfield[@code='0']",
                        namespaces=item.nsmap)
    if ppn_val is not None:
        if ppn_val.text is not None:
            return ppn_val.text
    return ""


for vd18_nr in online:
    if vd18_nr is not None:
        counter += 1
        online_id.append(vd18_nr)
        ppn = get_ppn(vd18_nr)
        if ppn is None:
            print("could not find ppn of", vd18_nr)
            continue
        item_data = vd18.request(ppn, FORMAT)
        online_data.append(item_data)
        item_title = "!NO DATA!"
        item_years = "!NO DATA!"
        item_publisher = "!NO DATA!"
        if 'title' in item_data:
            if type(item_data["title"]) == list:   # alternative titles present
                item_title = item_data["title"][0]
            else:
                item_title = item_data["title"]
        if 'publisher' in item_data:
            item_publisher = item_data["publisher"]
        if 'year' in item_data:
            item_years = item_data["year"]
            if len(item_years) > max_year:
                max_year = len(item_years)
        online_titles.append(item_title)
        online_years.append(item_years)
        online_publisher.append(item_publisher)
    else:
        online_data.append({})
        online_years.append("NO DATA!!")
        online_titles.append("NO DATA!!")
        online_publisher.append("NO DATA!!")


if counter > 0:
    print("ONLINE ACCESSIBLE WORKS PRINTED / PUBLISHED BY GEBAUER")
    print("")
    for i, item_title in enumerate(online_titles):
        if item_title == "NO DATA!!":
            continue
        item_years = online_years[i]
        if len(item_years) < max_year:
            item_years += (max_year-len(item_years)) * " "
        print(online_id[i], " ",
              item_years, " ",
              item_title, " ",
              online_publisher[i])
