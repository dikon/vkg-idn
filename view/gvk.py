#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Creator:          D. Herre
GitLab:      dikon/vkg-idn

Created:        2020-07-07
Last Modified:  2020-07-26
"""

from librair import schemas
from librair import interfaces

online = schemas.json.reader("data/gvk-online--printer-gebauer.json")
vd18 = interfaces.unapi.Client("http://unapi.k10plus.de", "gvk", "ppn")

online_id = []
online_data = []
online_years = []
online_titles = []
online_publisher = []
counter = 0
max_year = -1
max_id = -1

FORMAT = "jsmf-json"

for ppn in online:
    if ppn is not None:
        counter += 1
        online_id.append(ppn)
        if len(ppn) > max_id:
            max_id = len(ppn)
        item_data = vd18.request(ppn, FORMAT)
        online_data.append(item_data)
        item_title = "!NO DATA!"
        item_years = "!NO DATA!"
        item_publisher = "!NO DATA!"
        if 'title' in item_data:
            if type(item_data["title"]) == list:   # alternative titles present
                item_title = item_data["title"][0]
            else:
                item_title = item_data["title"]
        if 'publisher' in item_data:
            item_publisher = item_data["publisher"]
        if 'year' in item_data:
            item_years = item_data["year"]
            if len(item_years) > max_year:
                max_year = len(item_years)
        online_titles.append(item_title)
        online_years.append(item_years)
        online_publisher.append(item_publisher)
    else:
        online_data.append({})
        online_years.append("NO DATA!!")
        online_titles.append("NO DATA!!")
        online_publisher.append("NO DATA!!")


if counter > 0:
    print("ONLINE ACCESSIBLE WORKS PRINTED / PUBLISHED BY GEBAUER")
    print("")
    for i, item_title in enumerate(online_titles):
        if item_title == "NO DATA!!":
            continue
        item_id = online_id[i]
        if len(item_id) < max_id:
            item_id += (max_id-len(item_id)) * " "
        item_years = online_years[i]
        if len(item_years) < max_year:
            item_years += (max_year-len(item_years)) * " "
        print(item_id, " ",
              item_years, " ",
              item_title, " ",
              online_publisher[i])
